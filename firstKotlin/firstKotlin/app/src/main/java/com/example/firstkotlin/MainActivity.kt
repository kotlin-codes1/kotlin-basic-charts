package com.example.firstkotlin

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.firstkotlin.databinding.ActivityMainBinding
import com.ezatpanah.williamchart_youtube.charts.DonutChartActivity

class
MainActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.apply {
            binding.btnBarChart.setOnClickListener {
                // Start BarChartActivity when the button is clicked
                startActivity(
                    Intent(
                        this@MainActivity,
                        BarChartActivity::class.java
                    )
                )
            }

            // Set click listener for the Donut Chart button
            binding.btnDonutChart.setOnClickListener {
                // Create an Intent for DonutChartActivity, but not starting it
                Intent(
                    this@MainActivity,
                    DonutChartActivity::class.java
                )
            }
        }
    }

    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnBarChart.setOnClickListener {
            startActivity(Intent(this@MainActivity, BarChartActivity::class.java))
        }

        binding.btnDonutChart.setOnClickListener {
            startActivity(Intent(this@MainActivity, DonutChartActivity::class.java))
        }
    }
    */

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}





