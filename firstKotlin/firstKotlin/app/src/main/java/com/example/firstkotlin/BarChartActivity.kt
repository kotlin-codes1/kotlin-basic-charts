package com.example.firstkotlin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.firstkotlin.databinding.ActivityBarChartBinding

class BarChartActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBarChartBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBarChartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Define the data for the bar chart
        val barSet = listOf(
            "5.11.0" to 50F,
            "5.13.0" to 30F,
            "5.14.0" to 34F,
            "5.14.6" to 25F,
            "5.16.0" to 2F,
            "6.1.0" to 29F
        )

        // Define the duration of the animation
        val animationDuration = 1000L

        // Set the animation duration for the bar chart and animate it
        with(binding.barChart) {
            animation.duration = animationDuration
            animate(barSet)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
